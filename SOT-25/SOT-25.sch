EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L capstone_library:TCR2LF33 U1
U 1 1 5BB65EA7
P 5800 3250
F 0 "U1" V 6200 3150 50  0000 R CNN
F 1 "TCR2LF33" V 6300 3250 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 5800 3250 50  0001 C CNN
F 3 "" H 5800 3250 50  0001 C CNN
	1    5800 3250
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 5BB65F81
P 5500 3550
F 0 "J1" H 5400 3550 50  0000 C CNN
F 1 "Conn_01x03_Male" H 5500 3400 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5500 3550 50  0001 C CNN
F 3 "~" H 5500 3550 50  0001 C CNN
	1    5500 3550
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5BB65FB7
P 6450 3550
F 0 "J2" H 6423 3480 50  0000 R CNN
F 1 "Conn_01x03_Male" H 6750 3700 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6450 3550 50  0001 C CNN
F 3 "~" H 6450 3550 50  0001 C CNN
	1    6450 3550
	-1   0    0    1   
$EndComp
Wire Wire Line
	5800 3250 5800 3450
Wire Wire Line
	5800 3450 5700 3450
Wire Wire Line
	5900 3250 5900 3550
Wire Wire Line
	5900 3550 5700 3550
Wire Wire Line
	6000 3250 6000 3650
Wire Wire Line
	6000 3650 5700 3650
Wire Wire Line
	6100 3250 6100 3650
Wire Wire Line
	6100 3650 6250 3650
Wire Wire Line
	6200 3250 6200 3550
Wire Wire Line
	6200 3550 6250 3550
NoConn ~ 6250 3450
$EndSCHEMATC
