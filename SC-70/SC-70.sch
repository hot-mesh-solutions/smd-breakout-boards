EESchema Schematic File Version 4
LIBS:SC-70-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L capstone_library:TMP235A4DCKT U1
U 1 1 5BB64CC7
P 4500 2600
F 0 "U1" V 4740 2222 50  0000 R CNN
F 1 "TMP235A4DCKT" V 4649 2222 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-353_SC-70-5_Handsoldering" H 4500 2600 50  0001 C CNN
F 3 "" H 4500 2600 50  0001 C CNN
	1    4500 2600
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 5BB64E33
P 4150 2900
F 0 "J1" H 4256 3178 50  0000 C CNN
F 1 "Conn_01x03_Male" H 4150 2750 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4150 2900 50  0001 C CNN
F 3 "~" H 4150 2900 50  0001 C CNN
	1    4150 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 5BB64EC0
P 5100 2900
F 0 "J2" H 5073 2830 50  0000 R CNN
F 1 "Conn_01x03_Male" H 5500 3050 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 5100 2900 50  0001 C CNN
F 3 "~" H 5100 2900 50  0001 C CNN
	1    5100 2900
	-1   0    0    1   
$EndComp
Wire Wire Line
	4400 2600 4400 2800
Wire Wire Line
	4400 2800 4350 2800
Wire Wire Line
	4500 2600 4500 2900
Wire Wire Line
	4500 2900 4350 2900
Wire Wire Line
	4600 2600 4600 3000
Wire Wire Line
	4600 3000 4350 3000
Wire Wire Line
	4700 2600 4700 3000
Wire Wire Line
	4700 3000 4900 3000
Wire Wire Line
	4800 2600 4800 2900
Wire Wire Line
	4800 2900 4900 2900
NoConn ~ 4900 2800
$EndSCHEMATC
