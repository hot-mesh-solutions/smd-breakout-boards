EESchema Schematic File Version 4
LIBS:QFN-20-CC1101-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L capstone_library:CC1101RGP U1
U 1 1 5BC9EEC6
P 2700 3200
F 0 "U1" H 3200 3425 50  0000 C CNN
F 1 "CC1101" H 3200 3334 50  0000 C CNN
F 2 "capstone_footprints:CC1101RGP" H 2700 3200 50  0001 C CNN
F 3 "" H 2700 3200 50  0001 C CNN
	1    2700 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x10_Male J1
U 1 1 5BC9F094
P 2200 3600
F 0 "J1" H 2306 4178 50  0000 C CNN
F 1 "Conn_01x10_Male" H 2306 4087 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 2200 3600 50  0001 C CNN
F 3 "~" H 2200 3600 50  0001 C CNN
	1    2200 3600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x10_Male J2
U 1 1 5BC9F0EE
P 5950 3800
F 0 "J2" H 5923 3680 50  0000 R CNN
F 1 "Conn_01x10_Male" H 5923 3771 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 5950 3800 50  0001 C CNN
F 3 "~" H 5950 3800 50  0001 C CNN
	1    5950 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	2400 3200 2700 3200
Wire Wire Line
	2400 3300 2700 3300
Wire Wire Line
	2400 3400 2700 3400
Wire Wire Line
	2400 3500 2700 3500
Wire Wire Line
	2400 3600 2700 3600
Wire Wire Line
	2400 3700 2700 3700
Wire Wire Line
	2400 3800 2700 3800
Wire Wire Line
	2400 3900 2700 3900
Wire Wire Line
	2400 4000 2700 4000
Wire Wire Line
	2400 4100 2700 4100
Wire Wire Line
	5500 3300 5750 3300
Wire Wire Line
	5500 3400 5750 3400
Wire Wire Line
	5500 3500 5750 3500
Wire Wire Line
	5500 3600 5750 3600
Wire Wire Line
	5500 3700 5750 3700
Wire Wire Line
	5500 3800 5750 3800
Wire Wire Line
	5500 3900 5750 3900
Wire Wire Line
	5500 4000 5750 4000
Wire Wire Line
	5500 4100 5750 4100
Wire Wire Line
	5500 4200 5750 4200
Text Label 5500 3200 0    50   ~ 0
GND
Text Label 5500 3700 0    50   ~ 0
GND
Text Label 5500 3400 0    50   ~ 0
GND
$EndSCHEMATC
