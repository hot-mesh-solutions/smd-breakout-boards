EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "TSSOP-8"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L capstone_library:SPV1040 U1
U 1 1 5BBCCD58
P 5400 3600
F 0 "U1" H 5350 3100 50  0000 C CNN
F 1 "SPV1040" H 5331 3634 50  0000 C CNN
F 2 "Package_SO:TSSOP-8_4.4x3mm_P0.65mm" H 5400 3600 50  0001 C CNN
F 3 "" H 5400 3600 50  0001 C CNN
	1    5400 3600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J2
U 1 1 5BBCCDF9
P 6000 3900
F 0 "J2" H 6150 4100 50  0000 R CNN
F 1 "Conn_01x04_Male" V 5900 4150 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6000 3900 50  0001 C CNN
F 3 "~" H 6000 3900 50  0001 C CNN
	1    6000 3900
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 5BBCCE67
P 4750 3800
F 0 "J1" H 4850 3500 50  0000 C CNN
F 1 "Conn_01x04_Male" V 4650 3750 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4750 3800 50  0001 C CNN
F 3 "~" H 4750 3800 50  0001 C CNN
	1    4750 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3700 5800 3700
Wire Wire Line
	5600 3800 5800 3800
Wire Wire Line
	5600 3900 5800 3900
Wire Wire Line
	5600 4000 5800 4000
Wire Wire Line
	5100 3700 4950 3700
Wire Wire Line
	5100 3800 4950 3800
Wire Wire Line
	5100 3900 4950 3900
Wire Wire Line
	5100 4000 4950 4000
$EndSCHEMATC
